import attributes.PrimaryAttributes;
import characters.Warrior;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.*;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;


public class ItemTest {


    @Test
    public void TestEquipWeapon_ValidWeapon_ShouldPass() {
        Warrior warrior = new Warrior("LukeSkywalker");
        Weapon weapon = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);

        try {
            assertTrue(warrior.equip(weapon));
        } catch (InvalidWeaponException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void TestEquipWeapon_HigherRequiredLevel_ShouldThrowException() {
        Warrior warrior = new Warrior("LukeSkywalker");
        Weapon weapon = new Weapon("Common Axe", 2, Slot.WEAPON, WeaponType.AXE, 7, 1.1);

        assertThrows(InvalidWeaponException.class, () -> {
            warrior.equip(weapon);
        });
    }

    @Test
    public void TestEquipWeapon_WrongType_ShouldThrowException() {
        Warrior warrior = new Warrior("LukeSkywalker");
        Weapon weapon = new Weapon("Common Bow", 1, Slot.WEAPON, WeaponType.BOW, 12, 0.8);

        assertThrows(InvalidWeaponException.class, () -> {
            warrior.equip(weapon);
        });
    }

    @Test
    public void TestEquipWeapon_WrongSlot_ShouldThrowException() {
        Warrior warrior = new Warrior("LukeSkywalker");
        Weapon weapon = new Weapon("Common Bow", 1, Slot.HEAD, WeaponType.AXE, 12, 0.8);

        assertThrows(InvalidWeaponException.class, () -> {
            warrior.equip(weapon);
        });
    }

    @Test
    public void TestEquipArmor_ValidArmor_ShouldPass() {
        Warrior warrior = new Warrior("LukeSkywalker");
        PrimaryAttributes attributes = new PrimaryAttributes(1, 0, 2, 0);
        Armor armor = new Armor("Common Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, attributes);

        try {
            assertTrue(warrior.equip(armor));
        } catch (InvalidArmorException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void TestEquipArmor_HigherRequiredLevel_ShouldThrowException() {
        Warrior warrior = new Warrior("LukeSkywalker");
        PrimaryAttributes attributes = new PrimaryAttributes(1, 0, 2, 0);
        Armor armor = new Armor("Common Plate Body Armor", 2, Slot.BODY, ArmorType.PLATE, attributes);

        assertThrows(InvalidArmorException.class, () -> {
            warrior.equip(armor);
        });
    }

    @Test
    public void TestEquipArmor_WrongType_ShouldThrowException() {
        Warrior warrior = new Warrior("LukeSkywalker");
        PrimaryAttributes attributes = new PrimaryAttributes(1, 0, 2, 0);
        Armor armor = new Armor("Common Cloth Head Armor", 1, Slot.HEAD, ArmorType.CLOTH, attributes);

        assertThrows(InvalidArmorException.class, () -> {
            warrior.equip(armor);
        });
    }

    @Test
    public void TestEquipArmor_WrongSlot_ShouldThrowException() {
        Warrior warrior = new Warrior("LukeSkywalker");
        PrimaryAttributes attributes = new PrimaryAttributes(1, 0, 2, 0);
        Armor armor = new Armor("Common Cloth Head Armor", 1, Slot.WEAPON, ArmorType.PLATE, attributes);

        assertThrows(InvalidArmorException.class, () -> {
            warrior.equip(armor);
        });
    }

    @Test
    public void TestTotalAttributes_NoEquipment_ShouldGiveExpectedResult() {
        Warrior warrior = new Warrior("LukeSkywalker");
        PrimaryAttributes expected = new PrimaryAttributes(5, 2, 10, 1);
        PrimaryAttributes actual = warrior.getTotalPrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void TestTotalAttributes_WithArmor_ShouldGiveExpectedResult() {
        Warrior warrior = new Warrior("LukeSkywalker");
        PrimaryAttributes attributes = new PrimaryAttributes(1, 1, 2, 1);
        Armor armor = new Armor("Common Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, attributes);

        try {
            warrior.equip(armor);
        } catch (InvalidArmorException e) {
            e.printStackTrace();
        }

        PrimaryAttributes expected = new PrimaryAttributes(6, 3, 12, 2);
        PrimaryAttributes actual = warrior.getTotalPrimaryAttributes();

        assertEquals(expected, actual);
    }

    @Test
    public void TestDPS_NoWeapon_ShouldGiveExpectedResult() {
        Warrior warrior = new Warrior("LukeSkywalker");
        double expected = 1.05;
        double actual = warrior.getDPS();

        assertEquals(expected, actual);
    }

    @Test
    public void TestDPS_WithWeapon_ShouldGiveExpectedResult() {
        Warrior warrior = new Warrior("LukeSkywalker");
        Weapon weapon = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        try {
            warrior.equip(weapon);
        } catch (InvalidWeaponException e) {
            e.printStackTrace();
        }

        double expected = 8.09;
        double actual = warrior.getDPS();

        assertEquals(expected, actual);
    }

    @Test
    public void TestDPS_WithWeaponAndArmor_ShouldGiveExpectedResult() {
        Warrior warrior = new Warrior("LukeSkywalker");
        Weapon weapon = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
        PrimaryAttributes attributes = new PrimaryAttributes(1, 0, 2, 0);
        Armor armor = new Armor("Common Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, attributes);
        try {
            warrior.equip(weapon);
            warrior.equip(armor);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            e.printStackTrace();
        }

        double expected = 8.16;
        double actual = warrior.getDPS();

        assertEquals(expected, actual);
    }
}

