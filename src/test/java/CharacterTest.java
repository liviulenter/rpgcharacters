import attributes.PrimaryAttributes;
import characters.Mage;
import characters.Ranger;
import characters.Rogue;
import characters.Warrior;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CharacterTest {

    @Test
    public void TestCreateCharacter_NullInput_ShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> {
            Warrior warrior = new Warrior(null);
        });
    }

    @Test
    public void TestCreateCharacter_ValidInput_ShouldBeLevelOne() {
        Warrior warrior = new Warrior("LukeSkywalker");
        int level = warrior.getLevel();

        assertEquals(1, level);
    }

    @Test
    public void TestLevelUp_ValidInput_ShouldBeLevelTwo() {
        Warrior warrior = new Warrior("LukeSkywalker");
        warrior.levelUp(1);
        int level = warrior.getLevel();

        assertEquals(2, level);
    }

    @Test
    public void TestLevelUp_InvalidInput_ShouldThrowException() {
        Warrior warrior = new Warrior("LukeSkywalker");

        assertThrows(IllegalArgumentException.class, () -> {
            warrior.levelUp(-1);
        });
    }

    @Test
    public void TestCreateWarrior_ValidInput_ShouldHaveCorrectPrimaryAttributes() {
        Warrior warrior = new Warrior("LukeSkywalker");
        PrimaryAttributes expected = new PrimaryAttributes(5, 2, 10, 1);
        PrimaryAttributes actual = warrior.getBasePrimaryAttributes();
        assertEquals(expected, actual);
    }

    @Test
    public void TestCreateMage_ValidInput_ShouldHaveCorrectPrimaryAttributes() {
        Mage mage = new Mage("LukeSkywalker");
        PrimaryAttributes expected = new PrimaryAttributes(1, 1, 5, 8);
        PrimaryAttributes actual = mage.getBasePrimaryAttributes();
        assertEquals(expected, actual);
    }

    @Test
    public void TestCreateRanger_ValidInput_ShouldHaveCorrectPrimaryAttributes() {
        Ranger ranger = new Ranger("LukeSkywalker");
        PrimaryAttributes expected = new PrimaryAttributes(1, 7, 8, 1);
        PrimaryAttributes actual = ranger.getBasePrimaryAttributes();
        assertEquals(expected, actual);
    }

    @Test
    public void TestCreateRogue_ValidInput_ShouldHaveCorrectPrimaryAttributes() {
        Rogue rogue = new Rogue("LukeSkywalker");
        PrimaryAttributes expected = new PrimaryAttributes(2, 6, 8, 1);
        PrimaryAttributes actual = rogue.getBasePrimaryAttributes();
        assertEquals(expected, actual);
    }

    @Test
    public void TestLevelUpWarrior_ValidInput_ShouldHaveCorrectPrimaryAttributes() {
        Warrior warrior = new Warrior("LukeSkywalker");
        warrior.levelUp(1);
        PrimaryAttributes expected = new PrimaryAttributes(8, 4, 15, 2);
        PrimaryAttributes actual = warrior.getBasePrimaryAttributes();
        assertEquals(expected, actual);
    }

    @Test
    public void TestLevelUpMage_ValidInput_ShouldHaveCorrectPrimaryAttributes() {
        Mage mage = new Mage("LukeSkywalker");
        mage.levelUp(1);
        PrimaryAttributes expected = new PrimaryAttributes(2, 2, 8, 13);
        PrimaryAttributes actual = mage.getBasePrimaryAttributes();
        assertEquals(expected, actual);
    }

    @Test
    public void TestLevelUpRanger_ValidInput_ShouldHaveCorrectPrimaryAttributes() {
        Ranger ranger = new Ranger("LukeSkywalker");
        ranger.levelUp(1);
        PrimaryAttributes expected = new PrimaryAttributes(2, 12, 10, 2);
        PrimaryAttributes actual = ranger.getBasePrimaryAttributes();
        assertEquals(expected, actual);
    }

    @Test
    public void TestLevelUpRogue_ValidInput_ShouldHaveCorrectPrimaryAttributes() {
        Rogue rogue = new Rogue("LukeSkywalker");
        rogue.levelUp(1);
        PrimaryAttributes expected = new PrimaryAttributes(3, 10, 11, 2);
        PrimaryAttributes actual = rogue.getBasePrimaryAttributes();
        assertEquals(expected, actual);
    }

}
