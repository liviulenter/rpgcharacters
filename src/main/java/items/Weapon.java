package items;

public class Weapon extends Item {
    private int damage;
    private double attackSpeed;
    private WeaponType weaponType;

    public Weapon(String name, int requiredLevel, Slot slot, WeaponType weaponType, int damage, double attackSpeed) {
        super(name, requiredLevel, slot);

        if(damage < 1) {
            throw new IllegalArgumentException("Damage must be positive");
        }

        if(attackSpeed <= 0) {
            throw new IllegalArgumentException("Attack speed must be positive!");
        }

        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public double getDPS() {
        return damage * attackSpeed;
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "damage=" + damage +
                ", attackSpeed=" + attackSpeed +
                ", weaponType=" + weaponType +
                '}';
    }
}


