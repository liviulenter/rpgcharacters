package items;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
